package de.ragedev.maven;

public class ArrayParameterHelper {

	public static String merge(String[] repositoryUrls) {
		String result = "";
		
		for (String nextElement : repositoryUrls) {
			nextElement = nextElement.trim();
			if(nextElement.contains(","))
				throw new RuntimeException(nextElement + " contains semicolon. It's not allowed.");
			
			result += nextElement + ",";
			
		}
		
		return result.substring(0, result.length() - 1);
	}

}
