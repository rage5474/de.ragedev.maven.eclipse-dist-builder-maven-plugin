package de.ragedev.maven;

import java.io.File;
import java.io.IOException;

public class TmpFolderHelper {

	public static String getTmpFolderPath() {
		try {
			File temp = File.createTempFile("temp-file-name", ".tmp");

			String absolutePath = temp.getAbsolutePath();
			String tempFilePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));

			return tempFilePath;

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
