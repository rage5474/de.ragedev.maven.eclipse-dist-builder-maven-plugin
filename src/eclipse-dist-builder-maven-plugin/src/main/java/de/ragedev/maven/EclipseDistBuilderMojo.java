package de.ragedev.maven;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

@Mojo(name = "build")
public class EclipseDistBuilderMojo extends AbstractMojo {

	@Parameter(defaultValue = "eclipse-jee-neon-2-win32-x86_64.zip")
	private String eclipseZipName;

	@Parameter
	private String cacheFolder;

	@Parameter
	private String eclipseZipUrl;

	@Parameter
	private String[] repositoryUrls;

	@Parameter
	private String[] installUis;

	@Parameter(defaultValue = ".")
	private String destFolder;
	
	@Parameter(defaultValue = "false")
	private boolean destFolderOverride;

	public void execute() throws MojoExecutionException {
		checkIfInstallUisIsSet();
		checkIfRepositoryUrlIsSet();
		initCacheFolderIfNeeded();
		
		executeWithRollback();
		// TODO Cleanup?
		// TODO as zip?
		// TODO Release
		// TODO Blog
		// TODO Test on linux
		// TODO Download progress
		// TODO Check if download was successful
		// TODO installUIs as Array
		// TODO repositories as Array
	}
	
	private void executeWithRollback() throws MojoExecutionException
	{
		String source = cacheFolder + File.separator + eclipseZipName;
		File eclipseFolder = new File(cacheFolder + File.separator + "eclipse");
		try {

			downloadEclipseIfNeeded(source);

			extractEclipseInCacheFolder(source, eclipseFolder);

			waitOneSecond();

			installUiInEclipse(eclipseFolder);

			waitOneSecond();

			saveEclipseInDestFolder(eclipseFolder);
			removeFolderIfExists(eclipseFolder);
		} catch (Throwable e) {
			removeFolderIfExists(eclipseFolder);
			throw new MojoExecutionException("Execution failed.", e);
		}
		
	}

	private void checkIfInstallUisIsSet() throws MojoExecutionException {
		if (installUis == null) {
			throw new MojoExecutionException(
					"installUis tag must be set in pom configuration, e.g. <installUis>my.feature.group</installUis>. ");
		}
	}

	private void checkIfRepositoryUrlIsSet() throws MojoExecutionException {
		if (repositoryUrls == null || repositoryUrls.length == 0) {
			throw new MojoExecutionException(
					"repositoryUrls tag must be set in pom configuration, e.g. <repositoryUrls>http://download.eclipse.org/releases/neon</repositoryUrls>. ");
		}
	}

	private void initCacheFolderIfNeeded() {
		if (cacheFolder == null) {
			cacheFolder = TmpFolderHelper.getTmpFolderPath() + File.separator + "mvn-dist-builder";
		}
		getLog().info("Cache folder: " + cacheFolder);
	}

	private void downloadEclipseIfNeeded(String source) throws MojoExecutionException {
		File eclipseZipFile = new File(source);
		getLog().info("Following Eclipse zip found: " + eclipseZipFile.getAbsolutePath() );
		if (!eclipseZipFile.exists()) {

			URL website;
			try {
				if (eclipseZipUrl == null)
					eclipseZipUrl = "http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/neon/2/"
							+ eclipseZipName + "&r=1";

				getLog().info(eclipseZipName + " was not found in cache." );
				getLog().info("Starting download from " + eclipseZipUrl + " ...");
				getLog().info("Download can take a few minutes, so be patient. File will be cached for the next time. ");
				
				website = new URL(eclipseZipUrl);
				ReadableByteChannel rbc = Channels.newChannel(website.openStream());
				FileOutputStream fos = new FileOutputStream(source);
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
				fos.close();
				
				getLog().info("Download finished.");
				
			} catch (MalformedURLException e) {
				throw new MojoExecutionException("Downloading eclipse failed.", e);
			} catch (IOException e) {
				throw new MojoExecutionException("Downloading eclipse failed.", e);
			}
		}
	}

	private void extractEclipseInCacheFolder(String source, File eclipseFolder) throws MojoExecutionException {
		try {
			ZipFile zipFile = new ZipFile(source);
			zipFile.extractAll(cacheFolder);
			getLog().info("Eclipse extracted to " + eclipseFolder.getAbsolutePath() + ".");
		} catch (ZipException e) {
			throw new MojoExecutionException("Extracting Eclipse failed.", e);
		}
	}

	private void installUiInEclipse(File eclipseFolder) throws MojoExecutionException {
		Runtime rt = Runtime.getRuntime();
		String cmd = eclipseFolder.getAbsolutePath() + File.separator
				+ "eclipsec -application org.eclipse.equinox.p2.director -repository " + ArrayParameterHelper.merge(repositoryUrls) + " -installIU "
				+ ArrayParameterHelper.merge(installUis) + " -nosplash";
		getLog().info("Executing: " + cmd);
		try {
			final Process pr = rt.exec(cmd);
			new Thread(new Runnable() {
				public void run() {
					BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
					String line = null;

					try {
						while ((line = input.readLine()) != null) {
							getLog().info(line);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();

			try {
				pr.waitFor();
			} catch (InterruptedException e) {
				throw new MojoExecutionException("Installing feature in Eclipse failed.", e);
			}
		} catch (IOException e) {
			throw new MojoExecutionException("Installing feature in Eclipse failed.", e);
		}
	}

	private void waitOneSecond() throws MojoExecutionException {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			throw new MojoExecutionException("Waiting one second failed.", e);
		}
	}

	private void saveEclipseInDestFolder(File eclipseFolder) throws MojoExecutionException {
		try {
			getLog().info("Copy Eclipse distribution to destination folder: " + destFolder + " ...");
			
			File destFolderAsFile = new File(destFolder);
			if(destFolderAsFile.exists())
			{
				if(destFolderOverride)
				{
					getLog().info("Deleting destination folder " + destFolder + ".");
					FileUtils.deleteDirectory(destFolderAsFile);
					getLog().info("Deleting finished.");
				}
				else
				{
					getLog().error("Stopping because destination folder already exists. Considering to set <destFolderOverride>true</destFolderOverride> in pom configuration to override to folder.");
					throw new MojoExecutionException("Could not write to " + destFolder + ", because it already exists.");
				}
			}
			FileUtils.copyDirectory(eclipseFolder, destFolderAsFile);
			getLog().info("Copy Eclipse finished.");
		} catch (IOException e) {
			throw new MojoExecutionException("Saving eclipse in " + destFolder + " failed.", e);
		}
	}

	private void removeFolderIfExists(File eclipseFolder) throws MojoExecutionException {
		if (eclipseFolder.exists()) {
			getLog().info("Deleting old installation: " + eclipseFolder.getAbsolutePath());
			try {
				FileUtils.deleteDirectory(eclipseFolder);
			} catch (IOException e) {
				throw new MojoExecutionException("Removing extracted eclipse failed.", e);
			}
		}
	}

}