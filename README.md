# Eclipse Distribution Maven plug-in 

## Purpose
This Maven plug-in shall help to simply extend an Eclipse package, e.g. Eclipse for Java Developers, with other Eclipse features.

## Usage example
Add a similar section to your pom.xml
```xml
    ...
    <pluginRepositories>
		<pluginRepository>
			<id>OSS Sonatype Snapshots</id>
			<url>https://oss.sonatype.org/content/groups/public</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
				<enabled>false</enabled>
			</releases>
		</pluginRepository>
	</pluginRepositories>
    <plugin>
		<groupId>de.ragedev.maven</groupId>
		<artifactId>eclipse-dist-builder-maven-plugin</artifactId>
		<version>1.0-SNAPSHOT</version>
		<configuration>
			<repositoryUrls>
				<param>http://download.eclipse.org/releases/neon</param>
				<param>file:///E:/repositories/git/de.ragedev.example.dynamictoolbar/src/de.ragedev.eclipse.dynamictoolbar.tycho.p2/target/repository</param>
			</repositoryUrls>
			<installUis>
				<param>de.ragedev.eclipse.dynamictoolbar.feature.feature.group</param>
			</installUis>
			<destFolder>e:\tmp\my-eclipse</destFolder>
			<destFolderOverride>true</destFolderOverride>
			<eclipseZipName>eclipse-jee-neon-2-win32-x86_64.zip</eclipseZipName>
		</configuration>
		<executions>
			<execution>
				<phase>install</phase>
				<goals>
					<goal>build</goal>
				</goals>
			</execution>
		</executions>
	</plugin>
	...
```

## Notes
- Not released yet, snapshot version available, that's why plugin repository configuration is needed
- Only tested with Windows 7, but should be working on Linux also.

## How to build

**Precondition:**
- Maven > 3.0.0 needed.

**Procedure:**
- Clone
- Checkout
- Call mvn clean install in folder `src/eclipse-dist-builder-maven-plugin`

